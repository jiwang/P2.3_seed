# Introduction to Docker containers   


## a lecture for the course scientific data management by Stefano Piani 

## OUTLINE

This lecture presents the Docker technology.

Docker is a computer program that performs operating-system-level virtualization also known as containerization. 
Docker is primarily developed for Linux, where it uses the resource isolation features of the Linux kernel such as cgroups and kernel namespaces, and a union-capable file system such as OverlayFS and others[8] to allow independent "containers" to run within a single Linux instance,
avoiding the overhead of starting and maintaining virtual machines (VMs)

-- <cite>Wikipedia</cite>

The goal of this lecture is to give you an insight of what is Docker and how you can use it.

The first step is installing docker on your machine: install Docker CE (Comunity Edition) from one of the following links:

  * [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
  * [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
  * [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
  * [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)


If you are not using linux, your experience with Docker will be a little bit different. In any case, you can download it from:

  * [Windows](https://docs.docker.com/docker-for-windows/install/)
  * [Mac](https://docs.docker.com/docker-for-mac/install/)


If you are using a GNU system, but your distribution is not in the previous list, then [good luck](https://docs.docker.com/install/linux/docker-ce/binaries/) (I have never tested this kind of installation).


## References&Books

-  [https://docs.docker.com]

## additional materials

