How to install the NetCDF library on Docker
===========================================

We will install the NetCDF4 library with parallel I/O on top of HDF5 library.
The instructions can be found at https://www.unidata.ucar.edu/software/netcdf/docs/getting_and_building_netcdf.html#build_parallel

Step 0: Requirements
--------------------
Few packages are needed::
  
  apt-get install m4 curl libcurl4-gnutls-dev libcurl4-nss-dev libcurl4-openssl-dev

Note: if you have any errors, just install packages one by one.

Step 1: NetCDF package
----------------------

First of all, download and untar the package::

  wget https://github.com/Unidata/netcdf-c/archive/v4.6.0.tar.gz
  tar -zxvf v4.6.0.tar.gz
  cd netcdf-c-4.6.0

Step 3: Installation
---------------------

NetCDF-4 with parallel I/O will be built on top of HD55. Thus, it needs to know where 
the HDF5 libraries are. Let's set H5DIR to specify where parallel HDF5 was installed 
(you should know from the output message of your HDF5 installation)::

  export H5DIR=/usr/lib/x86_64-linux-gnu/hdf5/openmpi
  NCDIR=/usr/local
  CC=mpicc CPPFLAGS=-I${H5DIR}/include LDFLAGS=-L${H5DIR}/lib ./configure --enable-shared --enable-parallel-tests --prefix=${NCDIR}


NetCDF C Configuration Summary
--------------------------------

General::

  NetCDF Version:         4.6.0
  Configured On:          Fri Feb 23 11:59:01 UTC 2018
  Host System:            x86_64-pc-linux-gnu
  Build Directory:        /netcdf-c-4.6.0
  Install Prefix:         /usr/local

Compiling Options::

  C Compiler:             /usr/bin/mpicc
  CFLAGS:
  CPPFLAGS:               -I/usr/lib/x86_64-linux-gnu/hdf5/openmpi/include
  LDFLAGS:                -L/usr/lib/x86_64-linux-gnu/hdf5/openmpi/lib
  AM_CFLAGS:
  AM_CPPFLAGS:
  AM_LDFLAGS:
  Shared Library:         yes
  Static Library:         yes
  Extra libraries:        -lhdf5_hl -lhdf5 -ldl -lm -lz -lcurl 

Features::

  NetCDF-2 API:           yes

HDF4 Supp# NetCDF C Configuration Summary
-----------------------------------------

General::

  NetCDF Version:         4.6.0
  Configured On:          Fri Feb 23 11:59:01 UTC 2018
  Host System:            x86_64-pc-linux-gnu
  Build Directory:        /netcdf-c-4.6.0
  Install Prefix:         /usr/local

Compiling Options::

  C Compiler:             /usr/bin/mpicc
  CFLAGS:
  CPPFLAGS:               -I/usr/lib/x86_64-linux-gnu/hdf5/openmpi/include
  LDFLAGS:                -L/usr/lib/x86_64-linux-gnu/hdf5/openmpi/lib
  AM_CFLAGS:
  AM_CPPFLAGS:
  AM_LDFLAGS:
  Shared Library:         yes
  Static Library:         yes
  Extra libraries:        -lhdf5_hl -lhdf5 -ldl -lm -lz -lcurl 

Features::

  NetCDF-2 API:           yes
  HDF4 Support:           no
  NetCDF-4 API:           yes
  NC-4 Parallel Support:  yes
  PNetCDF Support:        no
  DAP2 Support:           yes
  DAP4 Support:           yes
  Diskless Support:       yes
  MMap Support:           no
  JNA Support:            no
  CDF5 Support:           no
  ERANGE fill Support:    no

After this, you should be able to install::

  make
  make check
  make install

Note: if you are running as root, you won't be able to pass 1 test. Access as your user and run make check again. 

At the end, check your configuration with::

  nc-config --all
  
You should see something similar to this::

  This netCDF 4.6.0 has been built with the following features:
  --cc        -> mpicc
  --cflags    -> -I/usr/local/include -I/usr/lib/x86_64-linux-gnu/hdf5/openmpi/include
  --libs      -> -L/usr/local/lib -lnetcdf
  --has-c++   -> no
  --cxx       -> 
  --has-c++4  -> no
  --cxx4      -> 
  --has-fortran-> no
  --has-dap    -> yes
  --has-dap4   -> yes
  --has-nc2    -> yes
  --has-nc4    -> yes
  --has-hdf5   -> yes
  --has-hdf4   -> no
  --has-logging-> no
  --has-pnetcdf-> no
  --has-szlib  -> yes
  --has-parallel -> yes
  --has-cdf5 -> no
  --prefix    -> /usr/local
  --includedir  -> /usr/local/include
  --libdir    -> /usr/local/lib
  --version   -> netCDF 4.6.0

Step 4: netCDF4-python installation
-----------------------------------

netCDF4-python is a Python interface to the netCDF C library.
We will use this simple interface from now on to illustrate the basic concepts in a netCDF file usage.
The API documentation is available at https://unidata.github.io/netcdf4-python
First of all, install the dependecies::

  python -m pip install --upgrade pip setuptools wheel

Then, clone the github repo from https://github.com/Unidata/netcdf4-python and enter the folder::

  cd  netcdf4-python

By default, the utility nc-config, installed with netcdf 4.1.2 or higher, will be run used to determine where all the dependencies live. If nc-config is not in your default $PATH edit the setup.cfg file in a text editor and follow the instructions in the comments. In addition to specifying the path to nc-config, you can manually set the paths to all the libraries and their include files (in case nc-config does not do the right thing). 

If you followed the installation steps up to here, you just need to edit the setup.cfg file and provide the required paths. 

If the netCDF C library was installed as a shared library in a location that is not searched by default, you will get some error message. In this case, you will need to set the LD_LIBRARY_PATH environment variable to specify that directory before running the configure script. For example::

  NCDIR=/usr/local
  export LD_LIBRARY_PATH=${NCDIR}/lib:${LD_LIBRARY_PATH}

After this, you are ready to build and install::

  python setup.py build
  python setup.py install

Check your installation::

  cd test/
  python run_all.py
  
  
Step 5: NetCDF Fortran installation
---------------------------------------------
Let's now install the NetCDF Fortran libraries. Check your LD_LIBRARY_PATH, and set it if needed::

  NCDIR=/usr/local
  export LD_LIBRARY_PATH=${NCDIR}/lib:${LD_LIBRARY_PATH}
  
Download the NetCDF-Fortran Release from https://www.unidata.ucar.edu/downloads/netcdf/index.jsp or just use the following commands::

  wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-fortran-4.4.4.tar.gz
  tar -zxvf netcdf-fortran-4.4.4.tar.gz
  cd netcdf-fortran-4.4.4

Set the right environment variables and run the configure::

  NFDIR=/usr/local
  CC=mpicc FC=mpif90 CPPFLAGS=-I${NCDIR}/include LDFLAGS=-L${NCDIR}/lib ./configure --prefix=${NFDIR}
  
Now you should be able to install it::

  make
  make check
  make install

The output of nf-config --all should be::

  This netCDF-Fortran 4.4.4 has been built with the following features: 
  --cc        -> gcc
  --cflags    ->  -I/usr/local/include -I/usr/local/include
  --fc        -> gfortran
  --fflags    -> -I/usr/local/include
  --flibs     -> -L/usr/local/lib -lnetcdff -L/usr/local/lib -lnetcdf -lnetcdf 
  --has-f90   -> no
  --has-f03   -> yes
  --has-nc2   -> yes
  --has-nc4   -> yes
  --prefix    -> /usr/local
  --includedir-> /usr/local/include
  --version   -> netCDF-Fortran 4.4.4


Step 6: installation on the cluster
-----------------------------------

NetCDF C Configuration Summary
-------------------------------

General::

  NetCDF Version:         4.6.0
  Configured On:          Thu Feb 22 15:17:30 CET 2018
  Host System:            x86_64-pc-linux-gnu
  Build Directory:        /u/exact/raversa/netcdf-c-4.6.0
  Install Prefix:         /u/exact/raversa

Compiling Options::

  C Compiler:             /opt/cluster/openmpi/1.10.2/gnu/4.8.3/bin//mpicc
  CFLAGS:
  CPPFLAGS:               -I/opt/cluster/hdf5-parallel/1.8.19/gnu/4.8.3/include
  LDFLAGS:                -L/opt/cluster/hdf5-parallel/1.8.19/gnu/4.8.3/lib
  AM_CFLAGS:
  AM_CPPFLAGS:
  AM_LDFLAGS:
  Shared Library:         yes
  Static Library:         yes
  Extra libraries:        -lhdf5_hl -lhdf5 -ldl -lm -lz 

Features::

  NetCDF-2 API:           yes
  HDF4 Support:           no
  NetCDF-4 API:           yes
  NC-4 Parallel Support:  yes
  PNetCDF Support:        no
  DAP2 Support:           no
  DAP4 Support:           yes
  Diskless Support:       yes
  MMap Support:           no
  JNA Support:            no
  CDF5 Support:           no
  ERANGE fill Support:    no
