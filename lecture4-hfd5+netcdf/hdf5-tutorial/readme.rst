To run the python notebooks from the docker container, you first need 
to install jupyter::

	pip install jupyter

Then you will need to know your IP with ifconfig::

	apt-get install net-tools
	ifconfig 

Now generate a config file and set a password, which will be ashed in 
/.jupyter/jupyter_notebook_config.json::

	jupyter notebook --generate-config
	jupyter notebook password

Now you are ready to run your notebook::

	jupyter-notebook --ip put.here.your.ip --no-browser	
