hint:
```
mpiexec -n [?] python [???]
```

distance.py
-----------
Suppose we have a file containing 1D dataset of coordinate pairs, 
and we want to compute their distance from the origin.
This is the kind of problem thai is trivial to parallelize, 
since each computation does not depend on the other. 

First, we create our file, containing the coordinates dataset, 
then compute the distance in parallel and write into the file.
The division of works is done implicitly (in this case using 
the process rank).

Note that a file opened with mpio can be safely accessed from 
multiple processes (collective operation). 

collective.py
-------------
This code shows a simple example of I/O operations: some can be 
performed collectively, while others must be performed independently.

1) The code has an error. Find it and correct it!

2) Try to run the code multiple times, and check the values of 
   the dataset. Are they always the same? Do they appear as you 
   would expect? Can you guess why? 

atomic.py
---------
This code shows how writing a file using MPI does not respect consistency.

1) Try to run it multiple times and check the output. 

2) Uncomment the comm.Barrier() and the f.atomic = True lines. 
   Does it change something?

3) Increase the size and check whether there are differences in 
   terms of performance between version 1) and version 2). 
