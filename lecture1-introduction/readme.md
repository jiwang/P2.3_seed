# Introduction to scientific data management 


## a lecture for the course scientific data management 

## OUTLINE

This lecture provides some introductory concepts about scientific data management 


## References&Books

- The FAIR Guiding Principles for scientific data management and stewardship [https://www.nature.com/articles/sdata201618]
- Book Scientific Data Management: Challenges, Technology, and Deployment 1st  Chapman & Hall/CRC ©2009 (available at Sissa Library)



## slides

https://github.com/sissa/P2.3_seed/blob/master/lecture1-introduction/L1-introduction.pdf

## additional materials

List of data repositories/data projects:

 - IRODS
 - FigShare (http://figshare.com)
 - Onedata https://onedata.org/#/home
 - Mendeley Data (https://data.mendeley.com/),
 - Zenodo (http://zenodo.org/), 
 - DataHub (http://datahub.io), 
 - DANS (http://www.dans.knaw.nl/)
 - EUDat (http://eudat.eu)
 - NFFA-EUROPE IDRP  (by CNR/IOM )
 - http://www.onegeology.org/ 
 - http://www.ebi.ac.uk/ena
 - NeuroMorpho.Org (largest collection of publicly accessible 3D neuronal reconstructions and associated metadata)

## Task to performed

Identify a data project (and/or a data convention and/or a data topics) to present to the class as part of  the final exam.

Rules:
 - 15 minutes presentation
 - max 15 slides, title included 
 - Topic to be defined by the end of this week (discuss with me) 
 - It could include topics touched on the other lectures (parallel filesystem high level library) 
 - It could be based on some previous experience/work if related with data management  
 - It could be a the first small step t for your final thesis  

## project assignment (as discussed in class) 

 - 
