/* Created by S. Cozzini and G.P. Brandino for the Course P1.6 Data Management @ MHPC
 * Last Revision by R. Aversa: February 2018
 */
#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#define FILESIZE 16 // how many integers you want to read

int main(int argc, char **argv){

	int rank, size, offset, bufsize;
	int i, *buf;

	MPI_File fh;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	
	if(rank<FILESIZE%size){
		bufsize = FILESIZE/size +1;
		offset = rank*(FILESIZE/size +1);
	}
	else{
		bufsize = FILESIZE/size;
                offset = ( (FILESIZE%size)*(FILESIZE/size+1)+(rank-FILESIZE%size)*(FILESIZE/size) );
	}
	
	buf=(int*) malloc(bufsize*sizeof(int));

	MPI_File_open(MPI_COMM_WORLD,"datafile",MPI_MODE_RDONLY,MPI_INFO_NULL,&fh);
	MPI_File_read_at(fh, offset*sizeof(int), buf, bufsize, MPI_INT, &status);

	for (i=0;i<bufsize;i++){
		printf("rank: %d, buf[%d]: %d \n", rank, i, buf[i]);
	}

	free(buf);
	MPI_File_close(&fh);
	MPI_Finalize();

	return 0;
}
