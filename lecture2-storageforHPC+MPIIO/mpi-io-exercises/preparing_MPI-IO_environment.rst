Setting up the MPI-IO enviroment in a docker container 
========================================================


We will setup/install and configure all the packages we need to use MPI/IO.
Docker container is based on a standard Ubuntu:latest


Step 0: basics
--------------------

First of all, let's update apt-get::

	apt-get update

Now install git::

	apt-get install git

Install C and Fortran compilers::

	apt-get install gcc gfortran

Install the basic tools::

	apt-get install wget python python2.7-dev python-pip
	pip install --upgrade pip

Step 1: openmpi
--------------- 

We will use the standard precompiled package::

	apt-cache search openmpi
	apt-get install libopenmpi1.10 libopenmpi-dev 


Step 2: parallel hdf5 library
------------------------------  

Also in this case we will use the standard precompiled parallel version of the library::

	apt-cache search hdf5
	apt-get install libhdf5-openmpi-10 libhdf5-openmpi-dev libhdf5-openmpi-10-dbg


Step 3: MPI4py
---------------

We will install it manually following instruction provided here: http://mpi4py.scipy.org/docs/usrman/install.html#requirements

First of all, download the package, and untar it::

	wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-3.0.0.tar.gz
	tar -xvzf mpi4py-3.0.0.tar.gz
	cd mpi4py-3.0.0

let's now check if our mpicc compiler is available::

	which mpicc 

It should be /usr/bin/mpicc. If so, let's go for compilation::

	python setup.py build

and once done let's install it::

	python setup.py install

and this should work::

	mpiexec --allow-run-as-root -n 5 python -m mpi4py.bench helloworld
	mpiexec --allow-run-as-root -n 5 python demo/helloworld.py


Step 4: h5py with parallel support
----------------------------------

We will follow instructions provided here: http://docs.h5py.org/en/latest/mpi.html

Let us clone the repo::

	git clone https://github.com/h5py/h5py.git
	cd h5py/

At this point let us check where is the hdf5 library is::

	h5pcc --showconfig 

Once identified the path (it should be /usr/lib/x86_64-linux-gnu/hdf5/openmpi), use it to configure the package::

	python setup.py configure --mpi --hdf5=/usr/lib/x86_64-linux-gnu/hdf5/openmpi

Probably this will rise an error, because some dipendencies are still missing::

	apt-get install cython
	pip install numpy unittest2

The output of the configure should be::

	Autodetected HDF5 1.8.16
	Summary of the h5py configuration
	Path to HDF5: '/usr/lib/x86_64-linux-gnu/hdf5/openmpi'
	HDF5 Version: '1.8.16'
	MPI Enabled: True
	Rebuild Required: True

If the HDF5 Version is '1.8.4' don't worry, it will autodetect the right one during build::

	CC=h5pcc python setup.py build

and finally install::

	python setup.py install
	
Now the output will be::

	Autodetected HDF5 1.8.16
	Summary of the h5py configuration
	Path to HDF5: '/usr/lib/x86_64-linux-gnu/hdf5/openmpi'
	HDF5 Version: '1.8.16'
	MPI Enabled: True
	Rebuild Required: False	

To avoid using --allow-run-as-root create a new user::

	adduser XXX
	su - XXX
	
Clone the repo as user XXX::

	git clone https://github.com/sissa/P2.3_seed.git

Try to test the installation with::

	cd lecture2-storageforHPC+MPIIO/mpi-io-exercises/python
	mpiexec -n 4 python demo.py

see the output, and explore the file created with h5dump. 

Hint: you should probably need to install the HDF5 tools::

	apt-get install hdf5-tools
	
