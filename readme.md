# Scientific Data Management 


## Stefano Cozzini / Rossella Aversa 

## OUTLINE

This repository contains the  materials/programs/assignements and workspaces for the
course **Scientific Data Management**.


### Schedule of the course: 

- Monday February 26: 9:30 - 11:00 SISSA, Room 130
  - 09:30 - 11:00 : [Introduction to scientific data management] 
  - 11:30 - 13:00 : [Introduction to scientific data management] 
  - 14:00 - 18:00 : Data Science Workshop, SISSA, Room 128
  
- Tuesday February 27:
  - 09:00 - 13:00 : Data Science Workshop, SISSA, Room 128
  - 14:00 - 16:00 : [Introduction to Docker containers] SISSA, Room 130
  - 16:00 - 17:00 : [Preparing the MPI/IO enviroment by means of docker] SISSA, Room 130

- Wednesday February 28: MHPC Lecture Room, Miramare Campus 

  - 09:30 - 11:00 : [Introduction to HPC & parallel IO] 
  - 11:30 - 13:00 : [MPI-IO tutorials and first exercises] 
  - 14:00 - 15:00 : [Practical session on MPI/IO with python]
  - 15:00 - 17:00 : [MPI-IO optional exercises]
  - additional materials: [More on MPI-IO] 
- Thursday March 1: MHPC Lecture Room, Miramare Campus
  - 09:30 - 11:00 : [Introducing HDF5 library] 
  - 11:30 - 13:00 : [Tutorial on HDF5 with python notebook]
  - 14:00 - 15:00 : [How to install NetCDF]
  - 15:30 - 16:00 : [Introducing NetCDF library]
  - 16:00 - 17:00 : [Practical session on NetCDF libraries]
- Friday March 2 : MHPC Lecture Room, Miramare Campus: [PFS and benchmarking]
  - 09:30 - 11:00 : [Parallel FS + Lustre] 
  - 11:30 - 13:00 : [benchmarking storage in different ways.. ] 
  - 14:00 - 17:00 : [Practical session: using netcdf parallel libraries and benchmarking]
- Thursday March 8: MHPC Lecture Room, Miramare Campus
  - 09:30 - 11:00 : [I/O profiling and benchmarking] 
  - 11:30 - 13:00 : [Practical session on benchmarking/profiling I/O intensive code]
  - 14:00 - 17:00 : Time to complete benchmarks and presentation 
- Friday March 9 : MHPC Lecture Room, Miramare Campus
  - 09:30 - 13:00 : Time to complete benchmarks and presentations
  - 14:00 - 16:00 : Final exam and presentations


[Introduction to scientific data management]: https://github.com/sissa/P2.3_seed/tree/master/lecture1-introduction
[Introduction to HPC & parallel IO]: https://github.com/sissa/P2.3_seed/tree/master/lecture2-storageforHPC+MPIIO

[Preparing the MPI/IO enviroment by means of docker]: https://github.com/sissa/P2.3_seed/blob/master/lecture2-storageforHPC%2BMPIIO/mpi-io-exercises/preparing_MPI-IO_environment.rst

[More on MPI-IO]: https://github.com/sissa/P2.3_seed/tree/master/lecture3-more-on-MPIIO

[Introducing HDF5 library]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf
[Introduction to Docker containers]: https://github.com/sissa/P2.3_seed/tree/master/lectureG-docker
[PFS and benchmarking]: https://github.com/sissa/P2.3_seed/tree/master/lecture5-hpc-storage
[Parallel FS + Lustre]: https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/L5-PFS-and-Lustre.pdf
[I/O profiling and benchmarking]: https://github.com/sissa/P2.3_seed/tree/master/lecture7-io-profiling
[benchmarking storage in different ways.. ]: https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/L6-io-benchmarking-suggestion.pdf
[Practical session on benchmarking/profiling I/O intensive code]: https://github.com/sissa/P2.3_seed/tree/master/lecture7-io-profiling/readme.md#practical_sessions
[MPI-IO tutorials and first exercises]: https://github.com/sissa/P2.3_seed/tree/master/lecture2-storageforHPC%2BMPIIO/mpi-io-exercises
[Introducing NetCDF library]: https://github.com/sissa/P2.3_seed/blob/master/lecture4-hfd5%2Bnetcdf/L4bisintro-to-netcdf.pdf
[Practical session on MPI/IO with python]: https://github.com/sissa/P2.3_seed/tree/master/lecture2-storageforHPC%2BMPIIO/mpi-io-exercises
[Tutorial on HDF5 with python notebook]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf/hdf5-tutorial
[Practical session on NetCDF libraries]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf/netcdf-exercises/netcdf
[How to install NetCDF]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf/netcdf-exercises
[Practical session: using netcdf parallel libraries and benchmarking]: https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/readme.md#prctical_sessions
[MPI-IO optional exercises]: https://github.com/sissa/P2.3_seed/tree/master/lecture2-storageforHPC%2BMPIIO/mpi-io-exercises#4---using-set_view-optional
