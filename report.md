# report of assignments

## assignment 1, MPI_I/O on exactlab

```
module load netcdf-fortran-parallel/4.4.4/gnu/4.8.3
```

#### working directory into 4 OSTs

```
cd /lustre/MHPC17/jiwang
mkdir test
lfs setstripe -c 4 test
```

#### testing matrix size
```
block:
    5e+4 by 5e+4
size:
    9.4 GB

rectangular:
    2.8e+4 by 8.4e+4
    8.4e+4 by 2.8e+4
size:
    8.8 GB
```
#### results with -np 24

* block matrix

| function | time (s) | routine time (s) | bandwidth |
|-----------|---------|-------------------|-------------|
| all write | 10.94 | 21.82 | 0.86 GB/s |
| collective write | 6.83 | 29.90 | 1.38 GB/s |
| all read | 1.126 | 2.37 | 8.35 GB/s |
| collective read | 10.47 | 27.12 | 0.90 GB/s |

* rectangular matrix

| function | time (s) | routine time (s) | bandwidth |
|-----------|---------|-------------------|-------------|
| all write | 14.19 | 32.63 |  0.62 GB/s |
| collective write | 6.12 | 23.37 |  1.44 GB/s |
| all read | 1.22 | 1.95 |  7.21 GB/s |
| collective read | 8.78 | 13.56 | 1.00 GB/s |

#### questions:

* "all write" is slower than "collective write", a naive guess would be that CPU0 (which is used as spokeman in cllective write) is closer to output end than average.

* "collective read" and "collective write" have similar bandwidth, which sounds reasonable.

* "all read" over-performs than the other functions, to which I have no explanation
