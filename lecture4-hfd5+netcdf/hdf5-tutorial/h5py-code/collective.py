""" Created by R. Aversa for the P2.3 Data Management course @ MHPC.
    Last revision by R. Aversa: February 2018
    Source: Python and HDF5 by Andrew Collette, O'Reilly 2013.
""" 


import h5py
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.rank

f = h5py.File("collective_test.hdf5", "w", driver='mpio', comm=comm)

# All processes participate when creating an object
dset = f.create_dataset("x", (100,), dtype='i')

# Only one process participating in a metadata operation
if rank == 0:
    dset.attrs["title"] = "Hello"

# Data I/O can be independent
if rank == 0:
    dset[...] = 42
else:
    dset[rank] = rank

f.close()
