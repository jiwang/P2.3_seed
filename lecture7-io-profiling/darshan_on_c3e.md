## How to use DARSHAN on C3E cluster 

### Load the module:

   ```>module load darshan```
   
It will automagically load for you the latest module of Darshan available on the cluster (3.1.5)

### To run the code with darshan library just do this
```
>env LD_PRELOAD=/opt/cluster/darshan/3.1.5/openmpi/1.10.2/gnu/4.8.3/lib/libdarshan.so  mpirun -np XX YOUR_MPI_APPLICATION
```
### Once the code completed execution, check the darshan log directory of the day. I.e. for today (03/07/2018)
```
>ls /lustre/darshan-logs/2018/3/7/*
/lustre/darshan-logs/2018/3/7/exact_read_distribute_id25465_3-7-35801-5158550534678226806_1.darshan
```   
### Use darshan tools to get information

#### to generate a pdf report:

```
 darshan-job-summary.pl /lustre/darshan-logs/2018/3/7/exact_read_distribute_id25465_3-7-35801-5158550534678226806_1.darshan
```

#### to get all detailed about I/O: beware loong output.

```
darshan-parser /lustre/darshan-logs/2018/3/7/exact_read_distribute_id25465_3-7-35801-5158550534678226806_1.darshan
```

#### darshan-parser has several options: 
```
exact@master provided_code]$ darshan-parser --help
Usage: darshan-parser [options] <filename>
    --all   : all sub-options are enabled
    --base  : darshan log field data [default]
    --file  : total file counts
    --file-list  : per-file summaries
    --file-list-detailed  : per-file summaries with additional detail
    --perf  : derived perf data
    --total : aggregated darshan field data
```

More information on the tools here:

http://www.mcs.anl.gov/research/projects/darshan/docs/darshan3-util.html#_analyzing_log_files

### Proposed exercise:

Play with darshan library and utilities on IOR and Mdtest  parallel benchmarks 


