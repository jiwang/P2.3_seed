# Introduction on I/O issues on HPC and MPI/IO


## a lecture for the course scientific data management 

## OUTLINE

This lecture discuss the I/O issues on modern HPC architecture and presents MPI-IO approach 


## References&Books

-  W. Gropp, T. Hoefler, R. Thakur, E. Lusk, "Using Advanced MPI - Modern Features of the Message-Passing Interface", Scientific and engineering computation series

## additional materials

## tools 

To play with exercises we need an MPI library with MPI/IO enabled.
We will install it within a docker container in order to have all the same environment.


## exercises

 [https://github.com/sissa/P2.3_seed/tree/master/lecture2-storageforHPC%2BMPIIO/mpi-io-exercises]

