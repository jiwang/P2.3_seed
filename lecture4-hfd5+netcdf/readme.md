#  HDF5 libraries  


## a lecture for the course scientific data management 

## OUTLINE

This lecture presents and discuss the HFD5 library 


## References&Books

-  Python and HDF5* by Andrew Collette, O'Reilly 2013

## additional materials

- python notebooks adapted from Python and HDF5 by Andrew Collette, O'Reilly 2013
- [python examples] on h5py 

## exercises 

- [HDF5 notebooks]
- [h5py codes]



[HDF5 notebooks]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf/hdf5-tutorial/HDF5-notebooks
[h5py codes]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf/hdf5-tutorial/h5py-code
[python examples]: https://github.com/sissa/P2.3_seed/tree/master/lecture4-hfd5%2Bnetcdf/hdf5-python-examples

