""" Created by R. Aversa for the P2.3 Data Management course @ MHPC.
    Last revision by R. Aversa: February 2018
    Source: Python and HDF5 by Andrew Collette, O'Reilly 2013.
""" 


import h5py
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.rank

with h5py.File("atomic_test.hdf5", "w", driver="mpio", comm=comm) as f:
#    f.atomic = True # Enables strict atomic mode

    dset = f.create_dataset("x", (100,), dtype='i')

    if rank == 0:
        dset[...] = 42

#    comm.Barrier()

    if rank == 1:
        print dset[...]
