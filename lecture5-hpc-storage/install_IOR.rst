How to install IOR on the cluster
==================================

First of all, load the modules::

  module load openmpi/1.10.2/gnu/4.8.3
  module load hdf5-parallel/1.8.19/gnu/4.8.3

You can clone the repo::
  
  git clone https://github.com/hpc/ior.git
  
or download the zip file::

  wget https://github.com/hpc/ior/archive/master.zip
  unzip master
  
Now from inside the ior directory::

  ./bootstrap
  ./configure --prefix=$HOME --with-lustre --with-hdf5
  make
  make install
  
To perform mdtest::

  cd bin/
  mpirun -np 8 mdtest -d <filesystem> -n 10 -i 200 -y -N 60 -a <API>

where <filesystem> can be lustre, NFS (your home), or local (/tmp)
while <API> can be POSIX, MPIIO, or HDF5. 

To know what you are doing, the -h can be useful::

  ./mdtest -h
  
To perform IOR benchmark::
  
  mpirun -np 8 ~/bin/ior -u -B -s 1 -d 5 -w -r -i 1 -e -t 1m -b 1g -vv -k -B -F -a <API> -o <filesystem>
  

