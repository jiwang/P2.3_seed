Scientific Data Formats
=======================


netCDF Solution
---------------

The netCDF library is a more array oriented storage format, which lacks some
of the features of the HDF5 library, but has a simple interface.
It is described as:

*... Software libraries and self-describing, machine-independent
data formats that support the creation, access, and sharing of
array-oriented scientific data ...*

It is available from Unidata, one of the University Corporation for
Atmospheric Research (UCAR)'s Community Programs (UCP).
The latest implementation of the netCDF file format can be found 
from Unidata_

   http://www.unidata.ucar.edu/software/netcdf

.. _Unidata: http://www.unidata.ucar.edu/software/netcdf


Disk Formats
------------

The file on disk can have different formats:

+ NetCDF 3 classic : binary file with maximum size of 2 GB
+ NetCDF 3 64 bit offset : binary file with max 4GB per variable
+ NetCDF 4 : HDF5 file format with netCDF API on top
+ NetCDF 4 Classic : HDF5 file format with some limitations

Data Model
----------

With respect to the HDF5 data model, the netCDF has similar basic objects

- *GROUP* : each file contains one or more groups. They have distinct
  namespaces, and can be considered equivalent to on disk directores.
  Any group can contains multiple GROUPs. If not defined a GROUP, all
  other objects are in the ROOT group.
- *DIMENSION* : an integer number, fixed or growing, defining a
  dimensionality
- *DATATYPE* : User defined or primitive data structure
- *VARIABLE* : The actual data which has any number of dimensions
  and any number of attrybutes and one *DATATYPE*
- *ATTRIBUTE* : any data type associated either to a *GROUP* or to
  a *VARIABLE*



Netcdf using python
---------------------
We will use the simple netCDF4_ Python interface from now on to illustrate
the basic concepts in a netCDF file usage.

   https://unidata.github.io/netcdf4-python

.. _netCDF4: <https://unidata.github.io/netcdf4-python>

Writing a netCDF file
---------------------

Unidata supports netCDF APIs in C, C++, Fortran, and Java. Different projects
provide netCDF API for Python , perl , Ruby , R , Matlab , IDL.

::

 >>> from netCDF4 import Dataset
 >>> import numpy as np
 >>> f = Dataset('test.nc', 'w', format='NETCDF4')
 >>> f.description = 'bogus example script'
 >>> subgroup = f.createGroup('subgroup')
 >>> n1 = f.createDimension('n1', 3)
 >>> n2 = f.createDimension('n2', 3)
 >>> var = subgroup.createVariable('var','i4',(n1._name,n2._name))
 >>> var.comment = 'A comment'
 >>> a = np.ones((3,3))
 >>> var[...] = a
 >>> f.close()

Reading data from a netCDF file
-------------------------------

::

 >>> from netCDF4 import Dataset
 >>> f = Dataset('test.nc')
 >>> subgroup = f.groups['subgroup']
 >>> var = subgroup.variables['var']
 >>> var.comment
 'A comment'
 >>> a = var[...]
 >>> a
 array([[1, 1, 1],
        [1, 1, 1],
        [1, 1, 1]], dtype=int32)
 >>> f.close()

netCDF file cli tools 
-----------------------

We can use the **ncdump** program fron the netCDF library to transform the
binary file content into a human readable format, and conversely the **ncgen**
program to create a netCDF binary file from a textual representation of a file.

The netCDF library provides a third program **nccopy** which can be used to
create a new file from an existing one, changing the on-disk format and the
chunking strategy.


ncdump: A few things to try
-----------------------------

Look at just the header information (also called the schema or metadata)

:: 
  $ ncdump -h mslp.nc

Store entire CDL output for use later in ncgen exercises

::
   $ ncdump mslp.nc > mslp.cdl

Look at header and coordinate information, but not the data:

::
   $ ncdump -c mslp.nc

Look at all the data in the file, in addition to the metadata:

:: 
 $ ncdump mslp.nc
Look at a subset of the data by specifying one or more variables:

::
   $ ncdump -v lat,time mslp.nc

Look at times in human-readable form:

::
    $ ncdump -t -v lat,time mslp.nc

Look at what kind of netCDF data is in the file (classic, 64-bit offset, netCDF-4, or netCDF-4 classic model)

::

 :$ ncdump -k mslp.nc


Try the ncgen utility
----------------------


Check a CDL file for any syntax errors: 

:: 
    $ ncgen mslp.cdl

Edit mslp.cdl and change something (name of variable, data value, etc.).  
Use ncgen to generate new binary netCDF file (my.nc) with your changes:

::
    $ ncgen -o  my.nc mslp.cdl 
    $ ncdump my.nc

Generate a C or Fortran, program which, when compiled and run, will create the binary netCDF file corresponding to the CDL text file

::
    $ ncgen -l f77 mslp.cdl > mslp.f77
    $ ncgen -l c  mslp.cdl > mslp.c

Try compiling and running one of those programs.


Try nccopy utility
-------------------

Compress variables in a test file, test.nc, by using nccopy. Then check if adding the shuffling option improves compression:

::
    $ nccopy -d1 test.nc testd1.nc        # compress data, level 1
    $ nccopy -d1 -s test.nc testd1s.nc    # shuffle and compress data

    $ ls -l test.nc testd1.nc testd1s.nc  # check results

Measure the time it takes to compress/decompress a file according to level 

:: 
  $ /sur/bin/time nccopy -d9 test.nc testd1.nc        # compress data, level 9
  $ /sur/bin/time nccopy -d1 test.nc testd1.nc        # compress data, level 1


Try reomte acccess 
--------------------


Look at what's in some remote data from an OPeNDAP server:    

::
    $ ncdump -c http://test.opendap.org/opendap/data/nc/3fnoc.nc

Copy 3 coordinate variables out of the file    

::
    $ nccopy "http://test.opendap.org/opendap/data/nc/3fnoc.nc?lat,lon,time" coords.nc

Copy subarray of variable u out of the file into a new netCDF file

::
    $ nccopy "http://test.opendap.org/opendap/data/nc/3fnoc.nc?u[2:5][0:4][0:5]" u.nc
    $ ncdump u.nc

Download just the variable named "Total_precipitation" and relevant metadata from an OPeNDAP server dataset into a netCDF file named precip.nc

:: 
    $ nccopy 'http://motherlode.ucar.edu/thredds/dodsC/fmrc/NCEP/GFS/Hawaii_160km/NCEP-GFS-Hawaii_160km_best.ncd?Total_precipitation' precip.nc



Lab
-------------

We will install netCDF library with MPI capability on top of the HDF5.

We will examine how to use netcdf library to perform parallel IO.

Different strategies must be evaluated, and we will examine how chunking
affects performances.

