""" Created by R. Aversa for the P2.3 Data Management course @ MHPC.
    Last revision by R. Aversa: February 2018
    Source: Python and HDF5 by Andrew Collette, O'Reilly 2013.
""" 


import h5py
from mpi4py import MPI
import numpy as np

N = 1000
comm = MPI.COMM_WORLD # communicator which links all our processes together
rank = comm.rank # number identifying the process
size = comm.size

with h5py.File("coords.hdf5", "w",  driver="mpio", comm=comm) as f:
    	coords_dset = f.create_dataset("coords", (N,2), dtype='f4')
    	coords_dset[...] = np.random.random((N,2))
    	distances_dset = f.create_dataset("distances", (N,), dtype='f4')
    	
    	# serves as offset
	if(rank<N%size):
		length = N/size +1
    		offset = rank*(N/size +1)
	else:
		length = N/size
		offset = ( (N%size)*(N/size+1)+(rank-N%size)*(N/size) )

	print (rank,offset,offset+length)	
    	coords = coords_dset[offset:offset+length] # Load process-specific data
	print coords.shape
    	results = np.sqrt(np.sum(coords**2, axis=1)) # Compute distances
	print results.shape
    	distances_dset[offset:offset+length] = results # Write process-specific data
