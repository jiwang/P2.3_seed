# Lecture 5: parallel Fs and Lustre + L6: benchmarking I/O system  


##   two lectures for the course in scientific data management 

## OUTLINE

The first lecture on overview of the concept of parallel FS, present then the Lustre parallel Fs and its usage.
The second lecture discusses how to benchmark a I/O storage sytems and presents some microbenchmarks to play with.
 
## SLIDES
 
- https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/L5-PFS-and-Lustre.pdf
- https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/L6-io-benchmarking-suggestion.pdf

## References&Books

- http://wiki.lustre.org/Main_Page and in particular the pdf below:
- http://wiki.lustre.org/images/6/64/LustreArchitecture-v4.pdf

- https://www.nics.tennessee.edu/computing-resources/file-systems/io-lustre-tips


## practical_sessions 

 - [parallel netcdf exercise]
 - [compile and run IOR + mdtest]
 - [compile and run iozone]
 - work in groups to perform some benchmarks


## GROUP 1

perform, by means of iozone a benchmarking analysis on all the OSTs belonging to available lustre FS (ulysses and C3HPC)


## GROUP 2

perform, by means of IOR, a benchmarking analysis on different of different I/O approaches: POSIX/HDF5/MPI-IO

## GROUP 3

perform a detailed benchmarking analysis/comparison on netcdf parallel codes (allwrite vs collect_and_write) with respect to

 - size of the grid
 - number of processor
 - striping of the FILE


## additional materials

- IOR HPC benchmark [GitHub] page
- [IOR user guide]


[GitHub]: https://github.com/LLNL/ior
[IOR user guide]: https://asc.llnl.gov/sequoia/benchmarks/IOR_User_Guide.pdf
[compile and run IOR + mdtest]: https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/install_IOR.rst
[compile and run iozone]: https://github.com/sissa/P2.3_seed/blob/master/lecture5-hpc-storage/install_iozone.rst#how-to-install-and-run-iozone
[parallel netcdf exercise]: https://github.com/sissa/P2.3_seed/blob/master/lecture4-hfd5%2Bnetcdf/netcdf-exercises/parallel_netcdf/Parallel_NETCDF-lab.rst#lab-on-parallel-netcdf
