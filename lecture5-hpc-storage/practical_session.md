#  Practical sessions 
 
## a practical session for  course scientific data management 

## OUTLINE

 - parallel netcdf exercise 
 - compile and run IOR + mdtest
 - compile and run iozone
 - work in groups to perform some benchmarks


## GROUP 1

perform, by means of iozone a benchmarking analysis on all the OSTs belonging to available lustre FS (ulysses and C3HPC)


## GROUP 2 

perform, by means of IOR, a benchmarking analysis on different of different I/O approaches: POSIX/HDF5/MPI-IO

## GROUP 3

perform a detailed benchmarking analysis/comparison on netcdf parallel codes (allwrite vs collect_and_write) with respect to

 - size of the grid
 - number of processor
 - striping of the FILE 

