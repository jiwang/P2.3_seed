How to run a GUI inside a docker container
==========================================

If you want to use tools like e.g. **hdfview** inside a docker container, 
you need to run a container with the following options::

  docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix  -e DISPLAY=$DISPLAY --name <name> image

From inside::

  apt-get update
  apt-get install hdfview
  
Now you can run **as user** (if you try to di it as root it gives you an error)::

  hdfview
