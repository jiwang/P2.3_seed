#  I/O profiling and benchmarking  


## a lecture for the course scientific data management 

## OUTLINE

This lecture give some information how to profile and benchmark I/O system on HPC platform and how to profile applications which requires/uses I/O resources 

## SLIDES

## practical_sessions

In this practical session we will profile some scientific application  by means of some i/o profilers

 - [how to use darshan profiler on C3HPC cluster] 
 - some application to test:
    - RegCM4 
    - GEOTOP
    - Q/E espresso (?)
    - ??

## References&Books
 
 - http://www.mcs.anl.gov/research/projects/darshan/docs/darshan3-runtime.html

## additional materials

 - http://www.mcs.anl.gov/research/projects/darshan/docs/darshan3-runtime.html
 
 [how to use darshan profiler on C3HPC cluster]: darshan_on_c3e.md
