(hint: use od -i to check binary file)
# Exercises

Codes are available in the provided_code directory

## 1 - writeFile_offset.c and readFile_offset.c

1. Compile both codes. 
2. Check the `writeFile_offset.c` code and understand what it is
   doing.

   Select the correct answer, assuming the code runs on 4 processes:

   a. It writes 16 integers from 0 to 15

   #### b. It writes 16 integers, each process writes 0,1,2,3 

   c. It breaks your computer

3. Run the code using 4 processes. Compare your answer to the previous
   question with the output, by means of `od -i` on the produced
   datafile.

result:
```
0000000           0           1           2           3
*
0000100
```

4. Modify the code in order to print all the 16 integer by each
   process

my solution:
```
//offset = rank*(N/size)*sizeof(int);
offset = rank*N*sizeof(int);

...

//MPI_File_write_at(fhw, offset, buf, (N/size), MPI_INT, &status);
MPI_File_write_at(fhw, offset, buf, N, MPI_INT, &status);
```

5. Modify the code such that each process print a slice of the `buf`
   array.

my solution:
```
MPI_File_write_at(fhw, offset, buf+rank*N/size, (N/size), MPI_INT, &status);
//MPI_File_write_at(fhw, offset, buf, (N/size), MPI_INT, &status);
```

6. Run the code using 1,2 and 3 processes. Is the code writing the 16
   integers in all cases?

answer:
```
not at -np 4, since N/size cutoff the last one
```

7. Fix the code to run on an arbitrary number of processes, such that
   each process write a slice of the `buf` array.  (Hint: use the
   loadbalacing strategy )

my solutioin:
```
	if(rank<N%size){
		length = N/size +1;
        	offset = rank*(N/size +1);
	}
        else{
                length = N/size;
                offset = ( (N%size)*(N/size+1)+(rank-N%size)*(N/size) );
        }

        MPI_File_open(MPI_COMM_WORLD, "datafile", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fhw);
        printf("Rank: %d, Offset: %d\n", rank, offset);
        MPI_File_write_at(fhw, offset*sizeof(int), buf+offset, length, MPI_INT, &status);
```

8. Check the readFile_offset.c code and understand what it is doing.
  
   Answer the following questions:

   a. How many data each process reads?
	
answer:
```
80bits
```

   b. What do you expect each process to read?

answer:
```
80bits/(rank size)
```

9. Fix the code, to read the correct amount of data, as in point 5.

question:
```
you want to read 16 integers or 80bits?
```
my guess:
```
I modify the code by assuming you want to read 16 integers,
offset and bufsize is defined the same as in writeFile routine.
```
  
10. (Homework) Try to compile and run on different systems and see if
    the results are consistent.

answer:
```
If count things by integers, yes it is portable, if count things by bits, sorry, not portable.
```


## 2 - writeFile_pointer.f90 and readFile_pointer.c

1. Compile both codes.

2. Check the writeFile_pointer.f90 code and understand what it is
   doing.
 
   Select the correct answer:

   a. It does exactly what writeFile_offset does 

   b. It writes 16*[number of process] integers

   c. It breaks your computer

3. Run the code. Compare your answer to the previous question with the
   output, by means of `od -i` on the produced datafile.

4. Modify the code such that it behaves as writeFile_offset as in 1-5

5. Run readFile_pointer and check that everything works correctly

anticipate:
```
instead of using MPI_read/write_file_at, these two routines use successively two functions:
MPI_file_seek and MPI_read/write_file, which logically the same as section 1. Meanwhile, we
are free from writing file_type, this works only when a contiguous reading of each processor.
```

## 3 - MPI-IO with python 

In the python directory there is some material to play with mpi4py MPI implementation for python. 
Repeat the previous exercises using Python (Hint: look for mpi4py.MPI.File methods, e.g. at [file_methods] or [file_IO])
 

## 4 - using set_view (Optional)

Code is available in the optional_code directory.

1. Take a look at the F90 code where `file_set_view` routine is introduced

2. Compile the code and compare results with output of writeFile_pointer.f90

3. Modify the code to use file_set_view using mpi derived data_type (MPI_TYPE_CONTIGUOUS or MPI_TYPE_VECTOR) and get the same results as the code you start from.

4. Modify the code to write the file using the following pattern (assuming you use 4 processes):

   1 2 11 12 21 22 31 32 3 4 13 14 23 24 33 34

   (hint: use MPI_TYPE_VECTOR)

my solution:
```
 call MPI_TYPE_SIZE(MPI_INTEGER, intsize,ierr) !get number of bytes of MPI_INTEGER
 disp = myrank * (BUFSIZE/2) * intsize
 call MPI_TYPE_VECTOR(2,BUFSIZE/2,mysize*2,MPI_INTEGER,filetype,ierr) !mysize is number of total ranks
 call MPI_TYPE_COMMIT(filetype,ierr)

 call MPI_FILE_SET_VIEW(thefile, disp, MPI_INTEGER, &
                       filetype, 'native', &
                       MPI_INFO_NULL, ierr)
```

## 5 - MPI_SubArray.c (Optional) 

Code is available in the optional_code directory.

1. Compile and run the MPI_SubArray.c code. The code is set now to run on a grid of 2x3 processes, and using a global matrix of 8x15. The code can
   just use a matrix such that the number of rows that is a multiple of the number of rows in the grid, and the number of columns that is multiple 
   of the number of columns in the grid.

   1. Check, by reading the code and by means of od -i, what the code is doing.

2. Modify the code such it can handle an arbitrary matrix and an arbitrary grid.
   In the provided_code folder you can find a binary file called datafile_10x17_grid_3x5.
   This is the expected output for a matrix with 10 rows and 17 columns, on a process grid with
   3 rows and 5 columns. Your code should be able to produce this file, with the above parameters.


[file_methods]: https://mpi4py.scipy.org/docs/apiref/mpi4py.MPI.File-class.html
[file_IO]: http://pages.tacc.utexas.edu/~eijkhout/pcse/html/mpi-io.html
