/* Created by S. Cozzini and G.P. Brandino for the Course P1.6 Data Management @ MHPC
 * Last Revision by R. Aversa: February 2018
 */

#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"
int main(int argc, char **argv){
	
	int i, rank, size, offset, length, N=16 ;
	int* buf=(int*) malloc(N*sizeof(int));

	MPI_File fhw;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	for ( i=0;i<N;i++){
	  buf[i] = i ;
  	}
  	
	if(rank<N%size){
		length = N/size +1;
		offset = rank*(N/size +1);
	}
	else{
		length = N/size;
		offset = ( (N%size)*(N/size+1)+(rank-N%size)*(N/size) );
	}

	MPI_File_open(MPI_COMM_WORLD, "datafile", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fhw);
	printf("Rank: %d, Offset: %d\n", rank, offset);
	MPI_File_write_at(fhw, offset*sizeof(int), buf+offset, length, MPI_INT, &status);
	free(buf);
	MPI_File_close(&fhw);
	MPI_Finalize();

	return 0;
}
